\function{pvm_send_obj}
\synopsis{Pack and send data objects}
\usage{pvm_send (Int_Type tid, Int_Type msgid, object [,...])}
\description
  This function is much like \var{pvm_psend} except that it
  sends additional type information with each object. Using
  this function paired with \var{pvm_recv_obj} simplifies
  sending aggregate data objects such as structures and removes
  the need for the receiver to specify datatypes explicitly.
\example

  To send a \slang structure to another process:
#v+
  variable obj = struct {name, x, y, data};
  ...
  pvm_send_obj (tid, msgid, obj);
#v-

\seealso{pvm_recv_obj, pvm_psend, pvm_unpack}
\done

\function{pvm_recv_obj}
\synopsis{Receive data objects from \var{pvm_send_obj}}
\usage{obj = pvm_recv_obj ()}
\description
  This function receives an object sent by \var{pvm_send_obj}
  and returns a slang object of the same type that was sent.
  It simplifies sending aggregate data types such as structures.
\example

  To receive a \slang object sent by another process
  via \ifun{pvm_send_obj}:
#v+
  obj = pvm_recv_obj ();
#v-
\seealso{pvm_send_obj, pvm_psend, pvm_unpack}
\done

\function{pvm_config}
\synopsis{Returns information about the present virtual machine configuration}
\usage{Struct_Type = pvm_config ()}
\description
  See the PVM documentation.
\example
#v+
  h = pvm_config ();
#v-
\seealso{pvm_kill}
\done

\function{pvm_kill}
\synopsis{Terminates a specified PVM process}
\usage{pvm_kill (Int_Type tid)}
\description
  See the PVM documentation.
\example
#v+
  pvm_kill (tid);
#v-
\seealso{pvm_config}
\done

\function{pvm_initsend}
\synopsis{Clear default send buffer and specify message encoding}
\usage{bufid = pvm_initsend (Int_Type encoding)}
\description
  See the PVM documentation.
\example
#v+
  bufid = pvm_initsend (PvmDataDefault);
#v-
\seealso{pvm_send}
\done

\function{pvm_pack}
\synopsis{Pack the active message buffer with arrays of prescribed data type}
\usage{pvm_pack (object)}
\description
  See the PVM documentation.
\example
#v+
  pvm_pack (x);
#v-
\seealso{pvm_unpack}
\done

\function{pvm_send}
\synopsis{Immediately sends the data in the active message buffer}
\usage{pvm_send (Int_Type, tid, Int_Type msgid)}
\description
  See the PVM documentation.
\example
#v+
  pvm_send (tid, msgid);
#v-
\seealso{pvm_recv}
\done

\function{pvm_recv}
\synopsis{Receive a message}
\usage{bufid = pvm_recv (Int_Type tid, Int_Type msgtag)}
\description
  See the PVM documentation.
\example
#v+
   bufid = pvm_recv (tid, msgtag);
#v-
\seealso{pvm_send}
\done

\function{pvm_unpack}
\synopsis{Unpack the active message buffer into arrays of prescribed data type}
\usage{item = pvm_unpack (Int_Type type_id, Int_Type num)}
\description
  See the PVM documentation.
\example
#v+
  item = pvm_unpack (type, num);
#v-
\seealso{pvm_pack}
\done

\function{pvm_psend}
\synopsis{Pack and send data}
\usage{pvm_psend (Int_Type tid, Int_Type msgid, object [,...])}
\description
  See the PVM documentation.
\example
#v+
  pvm_psend (tid, msgid, data);
#v-
\notes
  Unlike the \var{pvm_send} function in the PVM library, this function
  does not operate asynchronously.
\seealso{pvm_send, pvm_initsend, pvm_pack, pvm_recv}
\done

\function{pvm_addhosts}
\synopsis{Add one or more hosts to the PVM server}
\usage{Int_Type[] = pvm_addhosts (String_Type[] hosts)}
\description
  See the PVM documentation.
\example
#v+
  tids = pvm_addhosts (["vex", "verus", "aluche"]);
#v-
\seealso{pvm_addhosts, pvm_config, pvm_delhosts}
\done

\function{pvm_delhosts}
\synopsis{Delete one or more hosts from the PVM server}
\usage{pvm_delhosts (String_Type[] hosts)}
\description
  See the PVM documentation.
\example
#v+
  pvm_delhosts (["vex", "verus"]);
#v-
\seealso{pvm_delhosts, pvm_config, pvm_kill}
\done

