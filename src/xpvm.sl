require ("pvm");

static define _xpvm_init_trace ()
{
   variable xpvm_tid;

   xpvm_tid = pvm_gettid ("xpvm", 0);
   if (xpvm_tid <= 0)
     {
	vmessage ("XPVM not running, cannot trace");
	return;
     }

   % set self trace and output destinations and message codes
   pvm_setopt (PvmSelfTraceTid, xpvm_tid);
   pvm_setopt (PvmSelfTraceCode, 666);
   pvm_setopt (PvmSelfOutputTid, xpvm_tid);
   pvm_setopt (PvmSelfOutputCode, 667);

   % set future children's trace and output destinations and codes
   pvm_setopt (PvmTraceTid, xpvm_tid);
   pvm_setopt (PvmTraceCode, 666);
   pvm_setopt (PvmOutputTid, xpvm_tid);
   pvm_setopt (PvmOutputCode, 667);

   % generate default trace mask
   variable trace_mask = Pvmtmask_Type;

   pvm_tev_mask_init (trace_mask);
   pvm_tev_mask_set (trace_mask, TEV_MCAST);
   pvm_tev_mask_set (trace_mask, TEV_SEND);
   pvm_tev_mask_set (trace_mask, TEV_RECV);
   pvm_tev_mask_set (trace_mask, TEV_NRECV);

   % set self trace mask
   pvm_settmask (PvmTaskSelf, trace_mask);

   % set future children's trace mask
   pvm_settmask (PvmTaskChild, trace_mask);
}

public define xpvm_setup ()
{
   ERROR_BLOCK
     {
	_clear_error();
	return;
     }

   _xpvm_init_trace ();
}

provide ("xpvm");
